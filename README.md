# myDoctoPinger
myDoctoPinger is a small tool to call doctolib and send an SMS notification when there are availabilities

## Installation
### python module
You need to install request module using:
```bash
pip install request 
```

### Configuration
We use the Free API to send SMS notifications.
Rename PingerConfiguration_template.yml into PingerConfiguration.yml
Fill user and pin information (from your Free Mobile subscription)

## Usage
 Simply do:
 ```bash
python3  myDoctoPinger.py
```

Example of crontab running it every 20 minutes:
 ```bash
*/20 * * * * /usr/bin/python3 /home/ubuntu/dev/myDoctoPinger.py >> /home/ubuntu/dev/pinger.log 2>&1
```
