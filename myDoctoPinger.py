import requests
import json
import smtplib
# Import the email modules we'll need
from email.message import EmailMessage
import datetime
import time
import yaml

fullUrl = "https://partners.doctolib.fr/availabilities.json?start_date=2021-01-21&visit_motive_ids=2529270&agenda_ids=406334-411168-412669&insurance_sector=public&practice_ids=162579&limit=7&allowNewPatients=true&telehealth=false&profileId=212285&isOrganization=true&telehealthFeatureEnabled=false&vaccinationMotive=true&vaccinationDaysRange=26"

#a valid reponse:
# {"availabilities":[{"date":"2021-01-25","slots":[],"substitution":null},{"date":"2021-01-26","slots":[],"substitution":null},{"date":"2021-01-27","slots":[],"substitution":null},{"date":"2021-01-28","slots":[{"agenda_id":412669,"practitioner_agenda_id":null,"start_date":"2021-01-28T15:10:00.000+01:00","end_date":"2021-01-28T15:15:00.000+01:00","steps":[{"agenda_id":412669,"practitioner_agenda_id":null,"start_date":"2021-01-28T15:10:00.000+01:00","end_date":"2021-01-28T15:15:00.000+01:00","visit_motive_id":2529270},{"agenda_id":406334,"practitioner_agenda_id":null,"start_date":"2021-02-23T07:55:00.000+01:00","end_date":"2021-02-23T08:00:00.000+01:00","visit_motive_id":2529271}]}],"substitution":null},{"date":"2021-01-29","slots":[],"substitution":null},{"date":"2021-01-30","slots":[],"substitution":null},{"date":"2021-01-31","slots":[],"substitution":null}],"total":1}

class ResultData:
    totalDispo=0
    numberFutureVacc=0

    def __init__(self, iJsonReply):
        self.totalDispo = iJsonReply['total']
        if self.numberFutureVacc:
            self.numberFutureVacc = iJsonReply['number_future_vaccinations']
        else:
            self.numberFutureVacc = 0

    def isOpen(self):
        if self.totalDispo > 0:
            return True
        else:
            return False

#read yaml file with notification configuration
def readConfiguration(iFileName):
    with open(iFileName, "r") as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    return [cfg["notification"]["user"],cfg["notification"]["pin"]]

def callService(iURL):
    response = requests.get(iURL)
    if response:
  #      print(response.json()['number_future_vaccinations'])
        return response.json()
    else:
        return "Error"

def sendNotification(iUser, iPin, iAddMsg):
    #https://smsapi.free-mobile.fr/sendmsg?user=14528332&pass=NKhPqkQQ2ZsxTR&msg=toto
    x = datetime.datetime.now()

    messageToSend = x.strftime("%Y %b %d %A %H %M %S") + " " + iAddMsg
    myUrlSms = "https://smsapi.free-mobile.fr/sendmsg?user="+ iUser +"&pass="+ iPin + "&msg=" + messageToSend

    responseSms = requests.get(myUrlSms)
    if responseSms:
        print(responseSms)
        #return responseSms.json()
    else:
        print("Error in Sms Call")
        #return "Error in Sms Call"

# Process the answer from Doctolib, sned notification when necessary
def processDoctolibAnswer(iJson, iNotifConfig):
    named_tuple = time.localtime() 
    time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
    if iJson == "Error":
        sendNotification(myIdentifier, myPin,"Error")
    else:
        try:
            myResultData = ResultData(iJson)
        except:    
            sendNotification(myIdentifier, myPin, "Error in Json decoding...")
            print(time_string, "Doctolib à verifier" , myResultData.numberFutureVacc)
        else:    
            if myResultData.isOpen():
                print(time_string,"C'est ouvert!!!")
                sendNotification(myIdentifier, myPin, "Disponibilités!")
            else:
                #sendNotification(myIdentifier, myPin, "Pas de dispo...")
                print(time_string,"Il faut attendre - Futures vaccinations:" , myResultData.numberFutureVacc)
    

#Do the job
myNotifConfig = readConfiguration("PingerConfiguration.yml")
processDoctolibAnswer(callService(fullUrl), myNotifConfig)
